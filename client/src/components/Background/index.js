import React, { Component } from "react";
//import ReactDOM from "react-dom"; 
import "./background.css";
import { Spring } from "react-spring/renderprops";

class Background extends Component {
    render() {
        return (
            <Spring 
            from={{opacity: 0}}
            to={{opacity: 1}}
            config={{duration: 1000}}>
                {props => (
                    <div style={props}>
                        <video playsInLine autoPlay muted loop id="bgvid" src={require("./1023005212-hd.mov")} type="video/mov"></video>
                    </div>
                )}
            </Spring>
        )
    }
};

export default Background;