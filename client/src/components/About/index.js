import React, { Component } from "react";
//import ReactDOM from "react-dom"; 
import "./about.css";
//import { Spring } from "react-spring/renderprops";

class About extends Component {
    render() {
        return (
                <div className="rightSideContainAbout">
                <h2 className="fontSubTitleBodyAbout">Thank you for visiting!</h2>
                    <div className="hiddenAbout">
                    <div className="cardAbout">
                    <div className="containerAbout2">
                        <h2 className="fontSubTitleBodyAbout">About Sam:</h2>
                        <ul>
                            <li className="fontRegularAbout">A seasoned professional who brings over 20 years experience in sales, customer service and management, to his new role as a web developer.</li>
                            <li className="fontRegularAbout">Currently searching for his next career opportunity - Passionate about front-end development and design, Sam is hoping to find a position that allows him the opportunity to make an immediate, positive impact for a company, while also having the chance to continue to build upon his current programming skillset.</li>
                            <li className="fontRegularAbout">Sam resides in Melbourne, Florida, with his wife, Sarah, and their cat, Smokey.</li>
                        </ul>
                        <h2 className="fontSubTitleBodyAbout">Skills:</h2>
                        <div className="flexContainerAbout">
                            <div className="flexWrapRowAbout">
                                <div><i className="skillsIconAbout fab fa-html5"></i></div>
                                <div><i className="skillsIconAbout fab fa-css3-alt"></i></div>
                                <div><i className="skillsIconAbout fab fa-bootstrap"></i></div>
                                <div><i className="skillsIconAbout fab fa-js-square"></i></div>
                                <div><i className="skillsIconAbout fab fa-react"></i></div>
                                <div><i className="skillsIconAbout fab fa-node-js"></i></div>
                                <div><i className="skillsIconAbout fas fa-database"></i></div>
                            </div>
                            <ul>
                            <li className="fontRegularAbout">Strong understanding of HTML and CSS, as well as with associated styling frameworks, such as Bootstrap and Materialize.</li>
                            <li className="fontRegularAbout">Primary programming experience has been in JavaScript, with certification specializing in the MERN Stack (MongoDB / Express / React / Node).</li>
                            <li className="fontRegularAbout">Previous training as an audio engineer, with extensive experience recording and editing audio files on various digital audio workstations (ie: Pro Tools).  </li>
                        </ul>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
        )
    }
};

export default About;