import React, { Component } from "react";
//import ReactDOM from "react-dom"; 
import "./aboutbackup.css";
import { Spring } from "react-spring/renderprops";

class About extends Component {
    render() {
        return (
            <Spring 
            from={{marginTop: 2000}}
            to={{marginTop: 10}}
            config={{delay: 500, duration: 1000}}>
            {props => (
                <div style={props} className="rightSideContain">
                    <div>
                        <h2 className="fontSubTitleBody">About Me:</h2>
                        <ul>
                            <li className="fontReg">Currently resides in West Melbourne, Florida, U.S.A.</li>
                            <li className="fontReg">Lives with his wife, Sarah, and the world's coolest cat, Smokey.</li>
                            <li className="fontReg">Blah, Blah, Blah....gotta keep putting stuff here. Do I like the bullet
                        points? Not sure......hello</li>
                        </ul>
                    </div>
                    <div>
                        <h2 className="fontSubTitleBody">Skills:</h2>
                        <div className="flexContainer">
                            <div className="flexWrapRow">
                                <div className="hypertextCSS"><i className="skillsIcon fab fa-html5"></i></div>
                                <div className="hypertextCSS"><i className="skillsIcon fab fa-css3-alt"></i></div>
                                <div>
                                <ul>
                                    <li className="skillsFont">Strong foundation in both HTML5 and CSS3 properties and concepts</li>
                                    <li className="skillsFont">Experienced with various component libraries, including Bootstrap and
                                Materialize</li>
                                </ul>
                                </div>
                            </div>
                            <div className="flexWrapRow">
                                <div className="javascript"><i className="skillsIcon fab fa-js-square"></i></div>
                                <div>
                                <ul>
                                    <li className="skillsFont">Decent knowledge of primary JavaScript syntax and fundementals, such as functions, arrays, loops and objects</li>
                                    <li className="skillsFont">blah blah blah blah blah blah blah</li>
                                </ul>
                                </div>
                            </div>
                            <div className="flexWrapRow">
                                <div className="nodejs"><i className="skillsIcon fab fa-node-js"></i></div>
                                <div>
                                <ul>
                                    <li className="skillsFont">blah blah blah blah blah blah blah</li>
                                    <li className="skillsFont">blah blah blah blah blah blah blah</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>)}
                </Spring>
        )
    }
};

export default About;