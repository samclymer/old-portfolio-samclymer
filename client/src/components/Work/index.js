import React, { Component } from "react";
//import { Link } from 'react-router-dom';
//import ReactDOM from "react-dom"; 
import "./work.css";



class Work extends Component {
  render() {
    return (

      <div className="rightSideContainerWork">
        <h2 className="fontSubTitleWork">Portfolio:</h2>
        <div className="hiddenWork">
          <div class="cardWork">
            <div>
              <img alt="Movie Guess Game" src={require("./movieguessgame1.png")} />
            </div>
            <div>
              <h1 className="fontRegularWork"><b>Word Guess Game</b></h1>
              <ul className="ulSettings">
                <li className="skillsFontWork">Project designed to show understanding of concepts and technologies learned while enrolled in the University of Central Florida Web Development Certification Program.</li>
                <li className="skillsFontWork">Technology / Concepts Used: HTML / CSS / Bootstrap / JavaScript / jQuery / AJAX / Node / Express / MongoDB / MVC / Regular Expressions</li>
                <li className="skillsFontWork">Completion date: April 2019</li>
                <li className="skillsFontWork">Upcoming Improvements: Utilize Handlebars Helper to properly provide a specific number of top scores when pulling from database / Adjust the "Letters Guessed" alpha-numeric grid to become interactive for players to use when selecting letters, instead of being limited to keyboard input only</li>
                <li className="skillsFontWork">Visit website: <a href="https://www.movieguessgame.com">www.movieguessgame.com</a></li>
                <li className="skillsFontWork">Visit website repository: <a href="https://bitbucket.org/samclymer/ucfproject1/src/master/">UCFProject1 repository</a></li>
              </ul>
            </div>
          </div>
          <div class="cardWork">
            <div>
              <img alt="Sam Clymer Portfolio" src={require("./portfolio2.png")} />
            </div>
            <div>
              <h1 className="fontRegularWork"><b>Personal Porfolio</b></h1>
              <ul className="ulSettings">
                <li className="skillsFontWork">Project designed to show understanding of concepts and technologies learned while enrolled in the University of Central Florida Web Development Certification Program.</li>
                <li className="skillsFontWork">Technology / Concepts Used: HTML / CSS / JSX / React / React-Spring / Nodemailer</li>
                <li className="skillsFontWork">Completion date: March 2019</li>
                <li className="skillsFontWork">Upcoming Improvements: Completion of React-Spring transitions from component to component</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
};
export default Work;