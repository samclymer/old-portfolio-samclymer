import React, { Component } from "react";
//import ReactDOM from "react-dom"; 
import "./resume.css";
//import { Spring } from "react-spring/renderprops";

class ResumePDF extends Component {
  render() {
    return (
      <div className="rightSideContainerResume">
        <h2 className="fontSubTitleResume">Resume:</h2>
        <div className="hiddenResume">
          <div class="cardResume">

            <div className="containerResume2">
              <h2 className="fontBoxResume" style={{ textDecoration: `underline`}}>Work Experience</h2>
              <br />
              <h2 className="fontBoxResume">ICON Management Services</h2>
              <h2 className="skillsFontResume">Sarasota, Florida</h2>
              <br />
              <h2 className="fontRegularResume">Community Association Manager - River Strand Golf and Country Club</h2>
              <h2 className="skillsFontResume">May 2018 - October 2018</h2>
              <ul>
                <li className="fontRegularResume">Promoted in May 2018 to the role of Community Association Manager for River Strand Golf and Country Club.</li>
                <li className="fontRegularResume">Designated as System Administrator for River Strand's new club management and point-of-sale software systems.</li>
                <li className="fontRegularResume">Coordinated and managed meetings for the Board of Directors, as well as for the various committees of the Club (architectural, financial, security, compliance, and property management).</li>
                <li className="fontRegularResume">Responsibilities included the execution of directives as requested by the Board of Directors, hiring and oversight of multiple vendors, enforcement of the community's governing documents, and assistance with annual budgets and financial reporting.</li>
              </ul>
              <br />
              <h2 className="fontRegularResume">Assistant Manager of Food and Beverage - River Strand Golf and Country Club</h2>
              <h2 className="skillsFontResume">October 2016 - May 2018</h2>
              <ul>
                <li className="fontRegularResume">Assisted in the organization, management and administration of all
                operational aspects for the club’s food and beverage department.</li>
                <li className="fontRegularResume">Ensured all staff were meeting established standards of service. Monitored
                and tested service skills of the staff, and retrained and reinforced all standards
                on food and quality and service details daily. Provided feedback and
                appraisals as necessary.</li>
                <li className="fontRegularResume">Monitored and maintained cleanliness of dining rooms and work areas;
                communicated issues of safety, cleanliness or malfunctions to appropriate
                departments; managed maintenance/safety issues to completion.</li>
              </ul>
              <br />
              <h2 className="fontBoxResume">City Wide Maintenance</h2>
              <h2 className="skillsFontResume">Saint Petersburg, Florida</h2>
              <br />
              <h2 className="fontRegularResume">Outside Sales Executive</h2>
              <h2 className="skillsFontResume">May 2016 - October 2016</h2>
              <ul>
                <li className="fontRegularResume">Responsible for creating new accounts within both Sarasota and Manatee
Counties of Florida. Provided information and on-site presentations to
potential clients for over 20 different janitorial and facility maintenance
services.</li>
                <li className="fontRegularResume">Sold over $300,000 in annual contract revenue, and created over $1.5 M in
additional pipeline in less than 6 months within the position.</li>
                <li className="fontRegularResume">Broke the previous record for sales within the first month of employment for a
sales executive in the Tampa Bay branch.</li>
              </ul>
              <br />
              <h2 className="fontBoxResume">Kore</h2>
              <h2 className="skillsFontResume">Tampa, Florida</h2>
              <br />
              <h2 className="fontRegularResume">Business Development Representative | Account Executive</h2>
              <h2 className="skillsFontResume">October 2015 - March 2016</h2>
              <ul>
                <li className="fontRegularResume">Prospected and identified opportunities within large-enterprise accounts (20k
+ employees) throughout various verticals. Maintained over 100% quota on
monthly KPI's.</li>
                <li className="fontRegularResume">Utilized new techniques when prospecting for Kore’s pre-product launch
“Open House” WebEx presentation that ended up delivering over 80 C-Level
attendees from 17 different enterprise-level companies.</li>
                <li className="fontRegularResume">Produced a funnel of over $1.2 M in revenue within a 5-month period which
included companies such as Ford Motor Company, LabCorp, and Sykes
Enterprises.</li>
              </ul>
              <br />
              <h2 className="fontBoxResume">Innovative Technologies by Design (ITD Food Safety)</h2>
              <h2 className="skillsFontResume">Palm Bay, Florida</h2>
              <br />
              <h2 className="fontRegularResume">Account Executive | Implementation Specialist</h2>
              <h2 className="skillsFontResume">May 2014 - August 2015</h2>
              <ul>
                <li className="fontRegularResume">Provided automated food safety compliance solutions for nutrition directors
and food service managers within the healthcare, educational and hospitality
industries. Coordinated product trials for all major accounts and distribution
partners. Designed comprehensive programs for clients.</li>
                <li className="fontRegularResume">Outperformed all sales reps in company history within first month of
employment by leading complete transformation of sales approach; identified
new target market and made contact with appropriate decision-makers.</li>
                <li className="fontRegularResume">Negotiated and secured deals with notable clients such as QK Holdings
(largest Denny’s franchisee with 100 locations), A&W Restaurants, and
Stripes Convenience Stores. These contracts alone created over $1M in new
revenue.</li>
                <li className="fontRegularResume">Negotiated and secured deals with notable clients such as QK Holdings
                (largest Denny’s franchisee with 100 locations), A&W Restaurants, and
                Stripes Convenience Stores. These contracts alone created over $1M in new
revenue.</li>
                <li className="fontRegularResume">Built sales pipeline that was 2X current revenues.</li>
                <li className="fontRegularResume">Produced another $250,000 in sales to large K12 School Districts by
                customizing product programming to suit County-specific health and safety
guidelines.</li>
                <li className="fontRegularResume">Personally overhauled sales and training videos. Wrote copy, added voiceovers,
and created additional photos and footage.</li>
                <li className="fontRegularResume">Assigned responsibility for providing training and sales support to the
company’s field reps located throughout the country.</li>
              </ul>
              <br />
              <h2 className="fontBoxResume">Import/Export, Inc.</h2>
              <h2 className="skillsFontResume">Melbourne, Florida</h2>
              <br />
              <h2 className="fontRegularResume">Sales | Management</h2>
              <h2 className="skillsFontResume">October 2002 - May 2014</h2>
              <ul>
                <li className="fontRegularResume">Recruited to develop and maintain multiple high-volume accounts in the
automotive and retail industries. Transitioned into new role when auto bailout
wiped out existing business in 2008. Established and managed two retail
stores with full accountability for sales and profits. Trained and managed
warehouse and retail employees.</li>
                <li className="fontRegularResume">Brought in $250k in new business during the first year of employment (17%
of total sales) by conducting lead generation campaign consisting of cold calls,
direct mail campaigns and tradeshow/event marketing.</li>
                <li className="fontRegularResume">Grew new business to $400,000 annually over the following years, a 27%
increase in total sales.</li>
                <li className="fontRegularResume">Created and pioneered new apparel retail concept that allowed parents
                to sign up for a “Netflix-style” program to purchase high quality children’s
clothing.</li>
              </ul>
              <br />
              <h2 className="fontBoxResume">Alcon (Novartis)</h2>
              <h2 className="skillsFontResume">Orlando, Florida</h2>
              <br />
              <h2 className="fontRegularResume">Clinical Education Specialist</h2>
              <h2 className="skillsFontResume">August 2000 - September 2002</h2>
              <ul>
                <li className="fontRegularResume">Directed the coordination and implementation of laser training and
certification for ophthalmologists, managing a team of 10 field trainers.</li>
                <li className="fontRegularResume">Personally trained doctors. Demonstrated the correct techniques for cutting
corneas with the Summit Krumeich Barraquer Microkeratome, and became
a Certified LADARVision 4000 Excimer Laser Vision Correction System
Operator.</li>
                <li className="fontRegularResume">Managed through period of rapid growth that saw the hiring of 5 new field
trainers to ensure adequate coverage and optimal scheduling.</li>
              </ul>
              <br />
              <h2 className="fontBoxResume" style={{ textDecoration: `underline`}}>Education</h2>
              <br />
              <h2 className="fontBoxResume">University of Central Florida</h2>
              <h2 className="fontRegularResume">Full-Stack Web Development Certification</h2>
              <h2 className="skillsFontResume">2018 - 2019</h2>
              <br />
              <h2 className="fontBoxResume">University of Phoenix</h2>
              <h2 className="fontRegularResume">Bachelor of Science - Health Care Administration</h2>
              <h2 className="skillsFontResume">2007 - 2009</h2>
              <br />
              <h2 className="fontBoxResume">Valencia College</h2>
              <h2 className="fontRegularResume">Associate of Arts - General Studies</h2>
              <h2 className="skillsFontResume">2003 - 2004</h2>
              <br />
              <h2 className="fontBoxResume">Full Sail University</h2>
              <h2 className="fontRegularResume">Associate of Science - Audio Engineering</h2>
              <h2 className="skillsFontResume">1998 - 1999</h2>
            </div>
          </div>
        </div>
      </div>
    )
  }
};

export default ResumePDF;