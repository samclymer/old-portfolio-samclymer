import React, { Component } from "react";
//import ReactDOM from "react-dom"; 
import axios from "axios";
import "./form.css";

class Form extends Component {
    handleSubmit(e) {
        e.preventDefault();
        const fname = document.getElementById('fname').value;
        const lname = document.getElementById('lname').value;
        const email = document.getElementById('email').value;
        const phone = document.getElementById('phone').value;
        const message = document.getElementById('message').value;

        if (fname && lname && email && phone && message) {

        axios({
            method: "POST",
            url: "https://www.samclymer.com/send",
            data: {
                firstName: fname,
                lastName: lname,
                email: email,
                phone: phone,
                message: message
            }
        }).then((response) => {
            if (response.data.msg === 'success') {
                alert("Message Sent.");
                this.resetForm()
            } else if (response.data.msg === 'fail') {
                alert("Message failed to send.")
            }
        })
    }
    else  {
        alert("please fill out the entire form :)")
    }
    }

    resetForm() {
        document.getElementById('contact-form').reset();
    }

    render() {
        return (
                <div className="rightSideContainerForm">
                <h2 className="fontSubTitleBodyForm">Contact Sam:</h2>
                <div className="hiddenResume">
                <div className="cardResume">
                    <form id="contact-form" onSubmit={this.handleSubmit.bind(this)} method="POST" action="send">
                        <div>
                            <label className="fontRegularForm" for="fname">First Name:</label>
                        </div>
                        <div>
                            <input type="text" id="fname" name="firstname" placeholder="Your first name.." />
                        </div>
                        <br />
                        <div>
                            <label className="fontRegularForm" for="lname">Last Name:</label>
                        </div>
                        <div>
                            <input type="text" id="lname" name="lastname" placeholder="Your last name.." />
                        </div>
                        <br />
                        <div>
                            <label className="fontRegularForm" for="email">Email:</label>
                        </div>
                        <div>
                            <input type="email" id="email" name="email" placeholder="Your email.." />
                        </div>
                        <br />
                        <div>
                            <label className="fontRegularForm" for="phone">Phone:</label>
                        </div>
                        <div>
                            <input type="tel" id="phone" name="phone" placeholder="Your phone number.." />
                        </div>
                        <br />
                        <div>
                            <label className="fontRegularForm" for="message">Message:</label>
                        </div>
                        <br />
                        <div>
                            <textarea id="message" name="message" placeholder="Write something.."></textarea>
                        </div>
                        <br />
                        <div>
                            <button name="submit" type="submit" value="Submit">SUBMIT</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
        )
    }
};

export default Form;