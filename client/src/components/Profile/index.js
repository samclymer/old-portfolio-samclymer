import React, { Component } from "react";
//import ReactDOM from "react-dom"; 
import "./profile.css";
//import { Spring } from "react-spring/renderprops";
class Profile extends Component {
    render() {
        return (
            <div className="leftSideContainerProfile">
                <div className="imgContainerProfile">
                    <h1 className="fontTitleProfile">Sam Clymer</h1>
                    <h2 className="fontSubTitleLeftProfile">Web Developer</h2>
                    <img alt="Sam Clymer" className="headshotProfile" src={require("./headshot.jpg")} />
                </div>
                <div className="flexWrapRowSocialProfile">
                    <a href="https://www.linkedin.com/in/samclymer"><i id="socialIcon1" className="fab fa-linkedin"></i></a>
                    <a href="https://bitbucket.org/samclymer/ucfproject1/src/master/"><i id="socialIcon3" className="fab fa-bitbucket"></i></a>
                </div>
                <div className="imgContainerProfile">
                    <a href="https://credly.com/u/2608228"><img alt="UCF Credentials" className="headshotProfile" src={require("./badge.png")} /></a>
                </div>
            </div>
        )
    }
};

export default Profile;

/*<Spring
    from={{ marginLeft: -2000 }}
    to={{ marginLeft: 10 }}>
    {props => (
        <div style={props}></div>
                </div>)}
    </Spring>*/
