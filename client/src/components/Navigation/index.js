import React, { Component } from "react";
import { Link } from "react-router-dom";
//import ReactDOM from "react-dom"; 
import "./navigation.css";

class Navigation extends Component {
    render() {
        return (
            <div className="mainContainer">
                    <div className="navBar">
                        <Link to="/"><i id="navIcon1" className="fas fa-user"></i></Link>
                        <Link to="/resume"><i id="navIcon2" className="fas fa-file-alt"></i></Link>
                        <Link to="/portfolio"><i id="navIcon3" className="fa fa-code"></i></Link>
                        <Link to="/contact"><i id="navIcon4" className="fas fa-envelope"></i></Link>
                    </div>
                    </div>
        )
    }
};

export default Navigation;