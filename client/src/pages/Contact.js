import React from "react";
import Profile from "../components/Profile";
import Form from "../components/Form"
import Navigation from "../components/Navigation";
import "./pages.css";


function Contact() {
  return (
    <div>
      <div className="mainContainer">
      <div className="containerLeft">
          <Profile />
        </div>
        <div className="containerRight">
          <Form />
        </div>
      </div>
      <div className="navContainer">
        <Navigation />
      </div>
      </div>
  );
}

export default Contact;