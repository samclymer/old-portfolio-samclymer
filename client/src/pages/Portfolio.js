import React from "react";
import Profile from "../components/Profile";
import Navigation from "../components/Navigation";
import Work from "../components/Work"
import "./pages.css";


function Portfolio() {
  return (
    <div>
      <div className="mainContainer">
        <div className="containerLeft">
          <Profile />
        </div>
        <div className="containerRight">
          <Work />
        </div>
      </div>
      <div className="navContainer">
        <Navigation />
      </div>
    </div>
  );
}


export default Portfolio;