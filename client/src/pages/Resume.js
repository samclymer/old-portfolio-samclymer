import React from "react";
import Profile from "../components/Profile";
import ResumePDF from "../components/ResumePDF";
import Navigation from "../components/Navigation";
import "./pages.css";


function Resume() {
  return (
    <div>
      <div className="mainContainer">
        <div className="containerLeft">
          <Profile />
        </div>
        <div className="containerRight">
          <ResumePDF />
        </div>
      </div>
      <div className="navContainer">
        <Navigation />
      </div>
    </div>
  );
}

export default Resume;