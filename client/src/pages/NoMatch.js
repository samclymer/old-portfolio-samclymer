import React from "react";
import Background from "../components/Background";
import Navigation from "../components/Navigation";
import "./pages.css";

function NoMatch() {
    return (
        <div>
            <Background />
            <div className="mainContainer">
                <h1><strong>404 Page Not Found</strong></h1>
                <span role="img" aria-label="Face With Rolling Eyes Emoji">
                    🙄
              </span>
            </div>
            <div className="navContainer">
        <Navigation />
      </div>
            </div>
            
    );
}

export default NoMatch;