import React from "react";
import Profile from "../components/Profile";
import About from "../components/About";
import Navigation from "../components/Navigation";
import "./pages.css";

function Homepage() {
  return (
    <div>
      <div className="mainContainer">
        <div className="containerLeft">
          <Profile />
        </div>
        <div className="containerRight">
        <About />
        </div>
        </div>
      <div className="navContainer">
        <Navigation />
      </div>
    </div>
  );
}

export default Homepage;