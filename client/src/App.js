import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Homepage from "./pages/Homepage";
import Resume from "./pages/Resume";
import Portfolio from "./pages/Portfolio";
import Contact from "./pages/Contact";
import NoMatch from "./pages/NoMatch.js";
import Background from "./components/Background/index";


function App() {
    return (
      <Router>
        <div>
        <Background />
          <Switch>
            <Route exact path= "/" component={Homepage} />
            <Route exact path= "/resume" component={Resume} />
            <Route exact path= "/portfolio" component={Portfolio} />
            <Route exact path= "/contact" component={Contact} />
            <Route component={NoMatch} />
          </Switch>
        </div>
      </Router>
    );
}

export default App;
