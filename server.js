const express = require("express");
const path = require("path");
const PORT = process.env.PORT || 3001;
const app = express();
const nodemailer = require('nodemailer');
const bodyParser = require('body-parser');



// Serve up static assets (usually on heroku)
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
  app.get("*", function (req, res) {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
};

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Send every request to the React app
// Define any API routes before this runs


// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  service: "Hotmail",
  tls: {
    rejectUnauthorized: false
  },
  auth: {
    user: 'samclymer@outlook.com', 
    pass: '' 
  }
});
app.post('/send', (req, res) => {
  //console.log(req.body);
  const output = `
  <p>You have a new message!</p>
  <h3>Contact Details:</h3>
  <ul>
  <li>First Name: ${req.body.firstName}</li>
  <li>Last Name: ${req.body.lastName}</li>
  <li>Email: ${req.body.email}</li>
  <li>Phone: ${req.body.phone}</li>
  </ul>
  <h3>Message:</h3>
  <p>${req.body.message}</p>`;


  // setup email data with unicode symbols
  let mailOptions = {
    from: "Guest", // sender address
    to: "samclymer@gmail.com", // list of receivers
    subject: "Contact Form Email", // Subject line
    text: "Message:", // plain text body
    html: output // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      res.json({ msg: "error" });
    } else {
      res.json({ msg: "success" });
    };


  });
});

app.listen(PORT, function () {
  console.log(`🌎 ==> API server now on port ${PORT}!`);
});
